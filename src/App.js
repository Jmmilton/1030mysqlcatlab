import './App.css';
import CatProfile from "./CatProfile/catProfile"

function App() {
  return (
    <div className="App">
      <CatProfile />
    </div>
  );
}

export default App;
