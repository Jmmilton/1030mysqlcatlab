import db from "./src/db/connection"

const cors = require('cors');

const express = require("express")
const app = express();
app.use(cors())
app.use(express.json());

app.get("/api", (req, res) => {
    res.send("API Connected");
});

app.get("/api/cats", (req, res) => {
    db.query("SELECT * FROM cats", function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
        //connected
    })
})

const PORT = 3001
app.listen(PORT, () => 
    console.log(`API server ready on http://localhost:${PORT}`))